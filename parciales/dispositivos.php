<?php
require('../php/db.php');
include("../php/auth.php");

$username=$_SESSION['username'];
?>

<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Dispositivos | Pignus</title>

  <!-- Estilos Propios -->
  <link rel="stylesheet" href="../css/style.css"/>
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <!-- Font Awesome -->
  <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>

</head>

<body>
<?php
  if (isset($_POST['submit'])) {
    $ios = $_REQUEST['ios'];
    $android = $_REQUEST['android'];
    $linux = $_REQUEST['linux'];
    $macos = $_REQUEST['macos'];
    $windows = $_REQUEST['windows'];

    $update = "UPDATE users SET
    ios='".$ios."',  android='".$android."',  linux='".$linux."',  macos='".$macos."', windows='".$windows."' where username='".$username."'";

    mysqli_query($con, $update) or die(mysqli_error());
  }
?>
  <!-- Barra de navegación fijada arriba -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <a class="navbar-brand" href="index.php"><i class="fas fa-user-secret"></i></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
      <div class="navbar-nav">
        <a class="nav-item nav-link" href="../index.php">Home</a>
        <a class="nav-item nav-link" href="../parciales/noticias.html">Noticias</a>
        <a class="nav-item nav-link active" href="../parciales/dispositivos.php">Mis Dispositivos</a>
        <a class="nav-item nav-link" href="../parciales/seguridad.php">Seguridad</a>
        <a class="nav-item nav-link" href="../foro/index.php">Foro</a>
        <a class="nav-item nav-link" href="../php/logout.php">Salir</a>
      </div>
    </div>
  </nav>

  <!-- Contenido -->
  <div class="container-fluid"><h5 class="mt-2 text-center text-primary">Elige uno de los siguientes sistemas operativos: </h5></div>
  
  <form name="preferencias" method="post">
    <div class="container-fluid">
      <br>
      <div class="input-group mb-3">
        <div class="input-group-prepend">
          <div class="input-group-text bg-light">
            <input type="radio" name="ios" value="1"> Si<br>
            <input class="ml-2" type="radio" name="ios" value="0" checked> No<br>
          </div>
        </div>
        <input type="text" class="form-control bg-light" placeholder="iOS" disabled>
      </div>

      <div class="input-group mb-3">
        <div class="input-group-prepend">
          <div class="input-group-text bg-light">
            <input type="radio" name="android" value="1"> Si<br>
            <input class="ml-2" type="radio" name="android" value="0" checked> No<br>
          </div>
        </div>
        <input type="text" class="form-control bg-light" placeholder="Android" disabled>
      </div>

      <div class="input-group mb-3">
        <div class="input-group-prepend">
          <div class="input-group-text bg-light">
            <input type="radio" name="macos" value="1"> Si<br>
            <input class="ml-2" type="radio" name="macos" value="0" checked> No<br>
          </div>
        </div>
        <input type="text" class="form-control bg-light" placeholder="macOS" disabled>
      </div>

      <div class="input-group mb-3">
        <div class="input-group-prepend">
          <div class="input-group-text bg-light">
            <input type="radio" name="linux" value="1"> Si<br>
            <input class="ml-2" type="radio" name="linux" value="0" checked> No<br>
          </div>
        </div>
        <input type="text" class="form-control bg-light" placeholder="Linux" disabled>
      </div>

      <div class="input-group mb-3">
        <div class="input-group-prepend">
          <div class="input-group-text bg-light">
            <input type="radio" name="windows" value="1" > Si<br>
            <input class="ml-2" type="radio" name="windows" value="0" checked> No<br>
          </div>
        </div>
        <input type="text" name="linux" class="form-control bg-light" placeholder="Windows" disabled>
      </div>
    </div>

    <div class="text-center">
      <button type="submit" value="submit" name="submit" class="btn btn-primary">Guardar</button>
    </div>
  </form>

  <!-- JavaScript de Bootstrap -->
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>
