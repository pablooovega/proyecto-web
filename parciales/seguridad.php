<?php
require('../php/db.php');
include("../php/auth.php");

$username=$_SESSION['username'];
?>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Seguridad | Pignus</title>

  <!-- Estilos Propios -->
  <link rel="stylesheet" href="../css/style.css" />
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <!-- Font Awesome -->
  <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
</head>

<body>

  <!-- Barra de navegación fijada arriba -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top">
    <a class="navbar-brand" href="../index.php"><i class="fas fa-user-secret"></i></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
      <div class="navbar-nav">
        <a class="nav-item nav-link" href="../index.php">Home</a>
        <a class="nav-item nav-link" href="../parciales/noticias.html">Noticias</a>
        <a class="nav-item nav-link " href="../parciales/dispositivos.php">Mis Dispositivos</a>
        <a class="nav-item nav-link active" href="../parciales/seguridad.php">Seguridad</a>
        <a class="nav-item nav-link" href="../foro/index.php">Foro</a>
        <a class="nav-item nav-link" href="../php/logout.php">Salir</a>
      </div>
    </div>
  </nav>

  <!-- Contenido de la página -->
  <br><br><br>
  <?php
  $con->real_query("SELECT android, ios, macos, linux, windows FROM users");
  $resultado = $con->use_result();
  while ($row = $resultado->fetch_assoc()) {
    $ios = $row["ios"];
    $android = $row["android"];
    $macos = $row["macos"];
    $linux = $row["linux"];
    $windows = $row["windows"];
}


    if ($ios == 1) {
    header("Location: ../consejos/ios.html");
    exit();
  } elseif ($android == 1) {
    header("Location: ../consejos/android.html");
    exit();
  } elseif ($macos == 1) {
    header("Location: ../consejos/macos.html");
    exit();
  } elseif ($linux == 1) {
    header("Location: ../consejos/linux.html");
    exit();
  } elseif ($windows == 1) {
      header("Location: ../consejos/windows.html");
      exit();
  }
  ?>

  <p class="text-center">Elige un sistema operativo en la venta de "Mis dispositivos" para empezar a ver consejos</p>

  <!-- JavaScript de Bootstrap -->
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>
