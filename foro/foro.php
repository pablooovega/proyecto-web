<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Inicio | Pignus</title>

	<!-- Estilos Propios -->
	<link rel="stylesheet" href="css/style.css"/>
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<!-- Font Awesome -->
	<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
</head>

<body>
	<!-- Barra de navegación fijada arriba -->
	<nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top">
		<a class="navbar-brand" href="../index.php"><i class="fas fa-user-secret"></i></a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNavAltMarkup">
			<div class="navbar-nav">
				<a class="nav-item nav-link" href="../index.php">Home</a>
				<a class="nav-item nav-link" href="../parciales/noticias.html">Noticias</a>
				<a class="nav-item nav-link" href="../parciales/dispositivos.php">Mis Dispositivos</a>
				<a class="nav-item nav-link" href="../parciales/seguridad.php">Seguridad</a>
				<a class="nav-item nav-link active" href="index.php">Foro</a>
				<a class="nav-item nav-link" href="php/logout.php">Salir</a>
			</div>
		</div>
	</nav>

	<!-- Contenido -->
	<br><br><br>

	<div class="container-fluid">
		<?php
		include("conexionBD.php");
		if(isset($_GET["id"]))
		$id = $_GET['id'];
		$query = "SELECT * FROM  foro WHERE ID = '$id' ORDER BY fecha DESC";
		$result = $mysqli->query($query);
		while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
			$id = $row['ID'];
			$titulo = $row['titulo'];
			$autor = $row['autor'];
			$mensaje = $row['mensaje'];
			$fecha = $row['fecha'];
			$respuestas = $row['respuestas'];

			echo "<div class='mb-4 card'>
			<div class='card-header'>
			Autor: $autor
			</div>
			<div class='card-body'>
			<h5 class='card-title'>$titulo</h5>
			<p class='card-text'>$mensaje</p>
			<a href='formulario.php?id&respuestas=$respuestas&identificador=$id' class='btn btn-primary'>Responder</a>
			</div>
			</div>";
		}

		$query2 = "SELECT * FROM  foro WHERE identificador = '$id' ORDER BY fecha DESC";
		$result2 = $mysqli->query($query2);
		echo "<h6 class='ml-3'>Respuestas: </h6>";
		while($row = mysqli_fetch_array($result2, MYSQLI_ASSOC)){
			$id = $row['ID'];
			$titulo = $row['titulo'];
			$autor = $row['autor'];
			$mensaje = $row['mensaje'];
			$fecha = $row['fecha'];
			$respuestas = $row['respuestas'];

			echo "<div class='mt-2 ml-3 card'>
			<div class='card-header'>
			Autor: $autor
			</div>
			<div class='card-body'>
			<h5 class='card-title'>$titulo</h5>
			<p class='card-text'>$mensaje</p>
			</div>
			</div>
			";
		}

		echo "<br><div class='text-center'><a href='index.php' class='btn btn-outline-primary'>Volver</a></div><br>";
		?>
	</div> 
	


	<!-- JavaScript de Bootstrap -->
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>
