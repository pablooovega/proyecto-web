<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Inicio | Pignus</title>

  <!-- Estilos Propios -->
  <link rel="stylesheet" href="css/style.css"/>
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <!-- Font Awesome -->
  <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
</head>

<body>
  <!-- Barra de navegación fijada arriba -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top">
    <a class="navbar-brand" href="../index.php"><i class="fas fa-user-secret"></i></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
      <div class="navbar-nav">
        <a class="nav-item nav-link" href="../index.php">Home</a>
        <a class="nav-item nav-link" href="../parciales/noticias.html">Noticias</a>
        <a class="nav-item nav-link" href="../parciales/dispositivos.php">Mis Dispositivos</a>
        <a class="nav-item nav-link" href="../parciales/seguridad.php">Seguridad</a>
        <a class="nav-item nav-link active" href="index.php">Foro</a>
        <a class="nav-item nav-link" href="php/logout.php">Salir</a>
      </div>
    </div>
  </nav>

  <!-- Contenido -->
  <br><br><br>
  <h3 class="text-primary text-center">PignusForo</h3>
  <h6 class="mb-3 text-primary text-center">Haz cualquier pregunta sobre tu dispositivo: </h6>

  <table class="table table-striped">
		<thead class="thead-dark">
			<tr>
				<td width="20px"></td>
				<td width="200px">Título</td>
				<td width="200px">Respuestas</td>
			</tr>
		</thead>
    
	<?php
		include("conexionBD.php");
		$query = "SELECT * FROM  foro WHERE identificador = 0 ORDER BY fecha DESC";
		$result = $mysqli->query($query);
		while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
			$id = $row['ID'];
			$titulo = $row['titulo'];
			$fecha = $row['fecha'];
			$respuestas = $row['respuestas'];
			echo "<tr>";
				echo "<td><a class='mt-1 btn btn-outline-primary btn-sm' href='foro.php?id=$id'>Ver</a></td>";
				echo "<td><a class='btn text-dark' href='foro.php?id=$id'>$titulo</a></td>";
				// echo "<td>".date("d-m-y,$fecha")."</td>";
				echo "<td><a class='btn text-dark' href='foro.php?id=$id'>$respuestas</a></td>";
			echo "</tr>";
		}
	?>
	</table>


	<div class="mx-auto" style="text-align: center;">
		<button class="btn btn-primary"><a class="text-white" href="formulario.php">Nuevo Tema</a></button>
  </div>
  <br>




    <!-- JavaScript de Bootstrap -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
  </html>
