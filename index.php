<?php
include("php/auth.php");
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Inicio | Pignus</title>

  <!-- Estilos Propios -->
  <link rel="stylesheet" href="css/style.css"/>
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <!-- Font Awesome -->
  <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
</head>

<body>
  <!-- Barra de navegación fijada arriba -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top">
    <a class="navbar-brand" href="index.php"><i class="fas fa-user-secret"></i></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
      <div class="navbar-nav">
        <a class="nav-item nav-link active" href="index.php">Home</a>
        <a class="nav-item nav-link" href="parciales/noticias.html">Noticias</a>
        <a class="nav-item nav-link" href="parciales/dispositivos.php">Mis Dispositivos</a>
        <a class="nav-item nav-link" href="parciales/seguridad.php">Seguridad</a>
        <a class="nav-item nav-link" href="foro/index.php">Foro</a>
        <a class="nav-item nav-link" href="php/logout.php">Salir</a>
      </div>
    </div>
  </nav>S

  <!-- Contenido -->
  <br><br><br>
  <div class="container-fluid">
    <h4 class="text-center" style="color: #2d64f2">Bienvenido a Pignus!</h4>
    <h6 class="text-center" style="color: #2d64f2">Esto es todo lo que puede hacer:</h6>
  </div>

  <div class="container-fluid">

    <a href="parciales/dispositivos.php">
      <div class="card text-white bg-primary mb-3">
        <div class="card-header">Tus Dispositivos</div>
        <div class="card-body">
          <p class="card-text">Aqui podrás configurar los dispositivos que tienes, para que así recibas recomendaciones y tips solo de los dospitivos que tienes.</p>
        </div>
      </div>
    </a>

      <a href="parciales/noticias.html">
        <div class="card text-white bg-success mb-3">
          <div class="card-header">Noticias</div>
          <div class="card-body">
            <p class="card-text">La CiberSeguridad es importante, eso ya lo sabes todos, así aquí tendras todas las noticias relacionadas coon dicho tema.</p>
          </div>
        </div>
      </a>

      <a href="parciales/seguridad.php">
        <div class="card text-white bg-warning mb-3">
          <div class="card-header">Tips</div>
          <div class="card-body">
            <p class="card-text">Aquí apareceran todas la recomendaciones de seguridad relevelantes para ti, es decir las relacionadas con tus dispositivos. </p>
          </div>
        </div>
      </a>

      <a href="foro/index.php">
        <div class="card text-white bg-danger mb-3">
          <div class="card-header">Foro</div>
          <div class="card-body">
            <p class="card-text">Por mucho que recomendemos y aconsejemos, algo fallará... Así que aquí tienes esta sección para que nos podamos ayudar entre todos.</p>
          </div>
        </div>
      </a>
    </div>

    <!-- JavaScript de Bootstrap -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
  </html>
