<!DOCTYPE html>
<?php
require('db.php');
session_start();
// If form submitted, insert values into the database.
if (isset($_POST['username'])){
	// removes backslashes
	$username = stripslashes($_REQUEST['username']);
	//escapes special characters in a string
	$username = mysqli_real_escape_string($con,$username);
	$password = stripslashes($_REQUEST['password']);
	$password = mysqli_real_escape_string($con,$password);
	//Checking is user existing in the database or not
	$query = "SELECT * FROM `users` WHERE username='$username'
	and password='".md5($password)."'";
	$result = mysqli_query($con,$query) or die(mysql_error());
	$rows = mysqli_num_rows($result);
	if($rows==1){
		$_SESSION['username'] = $username;
		// Redirect user to index.php
		header("Location: ../index.php");
	}else{
		echo "<div class='form'>
		<h3>Username/password is incorrect.</h3>
		<br/>Click here to <a href='login.php'>Login</a></div>";
	}
}else{} ?>
<html>

	<head>
		<!-- Meta Tags -->
		<meta charset="utf-8">
		<meta name="apple-mobile-web-app-capable" content="yes" />
		<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>Pignus | Login</title>

		<!-- Estilos Propios -->
	  <link rel="stylesheet" href="../css/style.css" />
	  <!-- Bootstrap CSS -->
	  <link rel="stylesheet" href="../css/bootstrap.css">
	  <link rel="stylesheet" href="../css/bootstrap.css.map">
	  <!-- Font Awesome -->
	  <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
	</head>

	<body>
		<h1 class="text-center text-primary mt-5">Pignus</h1>
		<img id="login-photo" class="mt-3" src="../img/logo.png" onclick="sentidoNombre()" style="width: 200px; display: block; margin: auto;">

		<div class="container-fluid">

			<br>
			<form action="" method="post" name="login" style="margin: 0 auto; width:80%; text-align: center;">
				<div class="form-group">
					<label for="inputUser">Nombre de Usuario</label>
					<input id="inputUser" class="form-control" type="text" name="username" required />
				</div>

				<div class="form-group">
					<label for="inputPass">Contraseña</label>
					<input id="inputPass" class="form-control" type="password" name="password" required />
				</div>

				<div class="form-group mx-auto">
					<button name="submit" type="submit" value="Login" class="btn btn-primary" style="backgroud-color: pink !important;">Login</button>
				</div>
				<p class="mx-auto">¿Aún no estas registrado? <a href='registration.php'> Registrate Aquí</a><br></p>

			</form>
		</div>

		<script type="text/javascript">
			function sentidoNombre(){
				window.alert("¿Pignus? Vaya nombre... Pero todo tiene una razón. Pignus en latín significa seguridad y ese es el eje central de esta app.");
			}
		</script>
		<!-- JavaScript de Bootstrap -->
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	</body>

</html>
