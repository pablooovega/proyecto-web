<!DOCTYPE html>
<html>

  <head>
  <!-- Meta Tags -->
  <meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Estilos Propios -->
		<link rel="stylesheet" href="../css/style.css"/>
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
		<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
		<title>Pignus | Registro</title>
  </head>

  <body>
    <?php
    require('db.php');
    // If form submitted, insert values into the database.
    if (isset($_REQUEST['username'])){
      // removes backslashes
      $username = stripslashes($_REQUEST['username']);
      //escapes special characters in a string
      $username = mysqli_real_escape_string($con,$username);

      $email = stripslashes($_REQUEST['email']);
      $email = mysqli_real_escape_string($con,$email);

      $password = stripslashes($_REQUEST['password']);
      $password = mysqli_real_escape_string($con,$password);

      $trn_date = date("Y-m-d H:i:s");

      $query = "INSERT into `users` (username, password, email, trn_date)
      VALUES ('$username', '".md5($password)."', '$email', '$trn_date')";
      $result = mysqli_query($con,$query);

      if($result){
        echo "<div class='form mt-3'>
        <h4 class='text-center text-primary'>Te has registrado correctamentente</h4>
        <br/> <p class='text-center'>Haz click aqui para <a href='login.php'> Acceder</a></p></div>";
        }
    }else{
      ?>

      <h3 class="text-center text-info mt-5">Registrate en Pignus</h1>
      <img id="login-photo" class="mt-3" src="../img/logo.png" onclick="sentidoNombre()" style="width: 150px; display: block; margin: auto;">

      <div class="container-fluid">
			<br>
				<form name="registration" action="" method="post" style="margin: 0 auto; width:80%; text-align: center;">
					<div class="form-group">
						<label for="inputUser">Nombre de Usuario</label>
						<input id="inputUser" class="form-control" type="text" name="username" required />
					</div>
          <div class="form-group">
						<label for="inputUser">Mail</label>
						<input id="inputEmail" class="form-control" type="email" name="email" required />
					</div>
					<div class="form-group">
						<label for="inputPass">Contraseña</label>
						<input id="inputPass" class="form-control" type="password" name="password" required />
					</div>

					<div class="form-group mx-auto">
						<button name="submit" type="submit" value="Login" class="btn btn-info">Registrate</button>
					</div>

          <p class="mx-auto">Era broma, ya estoy registrado. <a href='login.php'> Login</a><br></p>

				</form>
		</div>
    <?php } ?>

    <!-- JavaScript de Bootstrap -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>
